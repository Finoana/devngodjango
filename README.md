# DevNgoDjango My_FAQ

Technologies:

Django 5
Django REST framework
SQLite
Repository:  https://gitlab.com/Finoana/devngodjango.git


Python 3.10+
pip
Installation:

Clone the repository:
git clone https://gitlab.com/Finoana/devngodjango.git

Create a virtual environment and activate it:
python3 -m venv venv
source venv/bin/activate
cd my_faq

Install dependencies:
pip install -r requirements.txt

Create the .env file and populate it with necessary database credentials.

Apply database migrations:
python manage.py migrate

Run the development server:
python manage.py runserver

The API will be accessible at http://localhost:8000/. Explore the available API endpoints using tools like Postman or curl.

Markdown
# My_FAQ - Django REST API for FAQs


This project is a Django 5 REST API for managing FAQs. It is built using the Django REST framework and leverages SQLite for data storage.

## Technologies

* Django 5
* Django REST framework
* SQLite

## Note
If need, create a superadmin django use 
python manage.py createsuperuser


## Installation

```bash
git clone [https://gitlab.com/Finoana/devngodjango.git](https://gitlab.com/Finoana/devngodjango.git)
# Create and activate virtual environment
python3 -m venv venv
source venv/bin/activate
cd my_faq
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver

Users: username / password
Jhon / azerty
Fy / azerty
john_doe / strong_password123
