from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import AnswerViewSet, QuestionViewSet, ThemeViewSet

router = DefaultRouter()
router.register(r"questions", QuestionViewSet)
router.register(r"answers", AnswerViewSet)
router.register(r"themes", ThemeViewSet)

urlpatterns = [
    path("faq/", include(router.urls)),
]