from rest_framework import viewsets, permissions
from rest_framework.response import Response
from .models import Question, Answer, Theme
from .serializers import QuestionSerializer, AnswerSerializer, ThemeSerializer
from rest_framework.decorators import action
from django.db.models import Max, Count
class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().select_related("author", "theme")
    serializer_class = QuestionSerializer
    
    def get_permissions(self):
        if self.action in ['create', 'list', 'retrieve']:
            return [permissions.AllowAny()]
        return [permissions.IsAuthenticated()]

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            query_response = self.queryset.all()
        else:
            query_response = self.queryset.filter(answers__isnull = False).distinct()
        
        return query_response.annotate(
            answer_count=Count('answers'),
            latest_answer_date=Max('answers__created_at')
        ).order_by('-latest_answer_date')
        
    def perform_create(self, serializer):
        author = None if not self.request.user.is_authenticated else self.request.user
        serializer.save(author=author)
    
    def retrieve(self, request, pk=None):
        try:
            question = self.get_object()
            answers = Answer.objects.filter(question=question)
            answer_serializer = AnswerSerializer(answers, many=True)
            return Response({
                "answers": answer_serializer.data
            })
        except Exception:
            return Response({'answers': []})
    
    @action(detail=False, methods=['get'], permission_classes = [permissions.IsAuthenticated])
    def no_answers(self, request):
        queryset = self.get_queryset().filter(answers__isnull=True)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

class AnswerViewSet(viewsets.ModelViewSet):
    queryset = Answer.objects.all().select_related("author", "question")
    serializer_class = AnswerSerializer
    permission_classes = [permissions.IsAuthenticated]  
    
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class ThemeViewSet(viewsets.ModelViewSet):
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer
    permission_classes = [permissions.AllowAny]
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        theme_name = request.data.get('theme')
        theme = Theme.objects.get_or_create(name=theme_name)[0] if theme_name else None
        serializer.save(theme=theme)
        return Response(serializer.data)


