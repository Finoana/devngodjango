from django.apps import AppConfig


class ManageQuestionsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'manage_questions'
