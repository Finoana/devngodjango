from django.contrib import admin

from .models import Question, Theme, Answer

admin.site.register(Question)
admin.site.register(Theme)
admin.site.register(Answer)