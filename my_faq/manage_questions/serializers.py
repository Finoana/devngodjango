from rest_framework import serializers
from .models import Question, Theme, Answer
from django.db.models.signals import post_save
from django.dispatch import receiver

class QuestionSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source="author.username")
    answer_count = serializers.SerializerMethodField()
    theme = serializers.SlugRelatedField(
        queryset=Theme.objects.all(),
        slug_field="name",
        required=False,
        allow_null=True,
    )

    class Meta:
        model = Question
        fields = (
            "id",
            "title",
            "content",
            "theme",
            "author",
            "created_at",
            "updated_at",
            "answer_count",
        )
        read_only_fields = ("answer_count",)
        
    def get_answer_count(self, obj):
        return obj.answers.count()

    @receiver(post_save, sender=Question)
    def update_question_answer_count(sender, instance, created, **kwargs):
        if created:
            instance.answer_count = instance.answers.count()
class AnswerSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source="author.username")

    class Meta:
        model = Answer
        fields = ("id", "question", "content", "author", "created_at", "updated_at")

from rest_framework import serializers
from .models import Theme

class ThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Theme
        fields = '__all__'  
       
