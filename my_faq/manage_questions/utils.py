import random
from django.utils.text import slugify

def generate_random_anonyme_alias():
    return slugify(f"Anonyme-{random.randint(1000, 9999)}")