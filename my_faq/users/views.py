from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from .serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token

class UserRegistrationView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=201)

class UserLoginView(ObtainAuthToken):
    permission_classes = [AllowAny] 

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'username': user.username, 'email': user.email}, status=200)

    def handle_invalid_credentials(self):
        return Response({'detail': 'Invalid username or password'}, status=401)


class UserLogoutView(APIView):
    def post(self, request):
        request.user.auth_token.delete()
        return Response(data={'message': 'Logged out.'}, status=200)
